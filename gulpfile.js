const fs = require('fs');
const gulp = require('gulp');
const flatten = require('gulp-flatten');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cssmin = require('gulp-cssmin');
const sass = require('gulp-sass');
const replace = require('gulp-replace');

const vendorDir = './vendor';
const distDir = './src/assets/dist';
const font = { src: vendorDir + '/**/*.{ttf,woff,woff2,eot,svg}', dest: './src/assets/dist/fonts' };
const css = { src: './src/assets/sass/*.scss', dest: './src/assets/dist/css' };

var vendorCss = [
    
]

var vendorJs = [

]

gulp.task('vendorCss', () => {
    gulp.src(vendorCss)
        .pipe(cssmin())
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest(distDir + '/css'))
})

gulp.task('vendorJs', () => {
    gulp.src(vendorJs)
        .pipe(uglify())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest(distDir + '/js'))
})

gulp.task('sass', function () {
    gulp.src(font.src)
        .pipe(flatten())
        .pipe(gulp.dest(font.dest));
    gulp.src(css.src)
        .pipe(sass({ outputStyle: 'compressed' }).on('error', sass.logError))
        .pipe(gulp.dest(css.dest));
});

gulp.task('clean-client-cache', function () {
    const version = new Date().getTime();
    return gulp.src('./src/index.html')
        .pipe(replace(/\/assets\/dist\/css\/app.css/g, `/assets/dist/css/app.css?${version}`))
        .pipe(replace(/\/assets\/dist\/css\/vendor.css/g, `/assets/dist/css/vendor.css?${version}`))
        .pipe(replace(/\/assets\/dist\/javascript\/vendor.js/g, `/assets/dist/javascript/vendor.js?${version}`))
        .pipe(gulp.dest('./src/'));
});

gulp.task('dev', ['vendorJs', 'vendorCss', 'sass']);
gulp.task('prod', ['vendorJs', 'vendorCss', 'sass', 'clean-client-cache']);