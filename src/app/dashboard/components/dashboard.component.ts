import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subscription } from 'rxjs/Rx';

@Component({
    selector: 'app-dashboard',
    templateUrl: './templates/dashboard.component.html',
})

export class DashboardComponent implements OnInit, OnDestroy {
    title: string = "Dashboard";
    subscription: Subscription;
    shopDomain: string;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
    ) {

    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.subscriptionInit();
    }

    subscriptionInit() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}