import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Observable, Subscription } from 'rxjs/Rx';
import { environment } from '../../../environments/environment';
import { BsModalService } from 'ngx-bootstrap/modal';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { ConfirmationModalComponent } from './modals/confirm.component';
import { PagerService } from '../../shared/services/pager.service';
import { GroupService } from '../../shared/services/group.service';

import * as _ from 'lodash';

@Component({
    selector: 'app-bundle-group',
    templateUrl: './templates/groups.component.html',
})

export class GroupsComponent implements OnInit, OnDestroy {
    title: string = "Quantity Breaks Groups";
    subs: Subscription;
    shopDomain: string;
    switchStatus = {
        onText: "",
        offText: "",
        onColor: "green",
        offColor: "default",
        size: "small",
        disabled: false,
    };

    disabledSyncButton: boolean = false;
    originBundleGroups: Array<any> = [];
    bundleGroups: Array<any> = [];

    // pager object
    pager: any = {};
    // paged items
    pagedItems: any[];
    // items per page
    itemsPerPage: number = 20;

    //query search
    queryString: string = '';

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private modalService: BsModalService,
        private messageService: MessageFlashService,
        private pagerService: PagerService,
        private groupService: GroupService,
    ) {

    }

    ngOnInit() {
        this.activatedRoute.data.subscribe(data => {
            this.shopDomain = data.shop;
            this.list();
        });
    }

    list() {
        this.subs = this.groupService.list(this.shopDomain).subscribe(results => {
            this.originBundleGroups = this.bundleGroups = _.orderBy(results, ['_id'], ['desc']);
            this.setPage(1);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    setPage(page: number) {
        // get pager object from service
        this.pager = this.pagerService.getPager(this.bundleGroups.length, page, this.itemsPerPage);
        // get current page of items
        this.pagedItems = this.bundleGroups.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    syncProducts(item: any): void {
        this.disabledSyncButton = true;
        item.syncBody = "Syncing";
        this.groupService.update(item._id, item.shop, {
            shop: item.shop,
            name: item.name,
            price_type: item.price_type,
            across_variants: item.across_variants,
            price_levels: item.price_levels,
            products: item.products,
            status: item.status,
        }).subscribe(res => {
            this.messageService.flashSuccess("Synchronize success.");
            this.disabledSyncButton = false;
            item.syncBody = null;
        }, err => {
            this.messageService.flashError(err);
        });
    }

    status(item) {
        if (item.status === 0) {
            return 'Off';
        }
        return 'On';
    }

    onStatus(item) {
        if (item.status !== 0) {
            return true;
        }
        return false;
    }

    onChangeStatus(status: boolean, item: any) {
        item.status = 0;
        if (status === true) {
            item.status = 1;
        }
        this.switchStatus.disabled = true;
        this.groupService.update(item._id, this.shopDomain, item).subscribe(() => {
            this.switchStatus.disabled = false;
            this.messageService.flashSuccess("Success");
        }, err => this.messageService.flashError(err));
    }

    delete(item) {
        const modal = this.modalService.show(ConfirmationModalComponent);
        (<ConfirmationModalComponent>modal.content).showConfirmationModal("Alert", "Delete this?");
        (<ConfirmationModalComponent>modal.content).onClose.subscribe(result => {
            if (result === true) {
                this.subs = this.groupService.remove(item._id, this.shopDomain).subscribe(() => {
                    this.messageService.flashSuccess('Success.');
                    this.list();
                }, err => this.messageService.flashError(err));
            }
        });
    }

    ngOnDestroy() {
        this.subsInit();
    }

    subsInit() {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

    querySearch() {
        let searchResults = [];
        let key = this.queryString.toLowerCase();
        if (!key) {
            this.bundleGroups = this.originBundleGroups;
        }
        this.originBundleGroups.forEach(bundle => {
            const bundleName = bundle.name.toLowerCase();
            if (bundleName.indexOf(key) != -1) {
                return searchResults.push(bundle);
            }
            bundle.products.forEach(product => {
                const productName = product.title.toLowerCase();
                if (productName.indexOf(key) != -1) {
                    return searchResults.push(bundle);
                }
            });
        });
        this.bundleGroups = searchResults;
        this.setPage(1);
    }
}