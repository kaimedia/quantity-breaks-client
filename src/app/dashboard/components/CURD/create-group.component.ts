import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { Location } from '@angular/common';
import { Observable, Subscription } from 'rxjs/Rx';
import { BsModalService } from 'ngx-bootstrap/modal';
import { CookieService } from "../../../core/services/cookie.service";
import { MessageFlashService } from '../../../shared/services/message-flash.service';
import { AddProductModalComponent } from '../modals/add-products.component';
import { GroupService } from '../../../shared/services/group.service';
import * as _ from 'lodash';

@Component({
    selector: 'create-group',
    templateUrl: './templates/create-group.component.html',
})

export class CreateGroupComponent implements OnInit, OnDestroy {
    title: string = "Create New Group";
    subs: Subscription;
    loading: boolean = false;
    btnSave: string = "Save";
    shopDomain: string;
    groupID: string;
    qbGroup: any = {
        shop: "",
        name: "",
        price_type: "%",
        across_variants: 'variant',
        price_levels: [],
        products: "",
        status: 1,
    }
    addQty: number;
    addValue: number;
    qtyArr: Object = {};

    constructor(
        private _location: Location,
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageFlashService,
        private cookieService: CookieService,
        private modalService: BsModalService,
        private groupService: GroupService,
    ) {

    }

    ngOnInit() {
        this.activatedRoute.data.subscribe(data => {
            if (data.title) this.title = data.title;
            this.shopDomain = data.shop;
            this.activatedRoute.params.subscribe(params => {
                if (params.id) {
                    this.groupID = params.id;
                    this.groupService.get(this.groupID, this.shopDomain).subscribe(result => {
                        result.products = _.map(result.products, p => {
                            if (!p.recommend) p.recommend = {
                                image: '',
                                text: '',
                                show: false,
                            };
                            return p;
                        });
                        this.qbGroup = result;
                        this.quantityModify();
                    }, err => {
                        this.messageService.flashError(err);
                    });
                } else {
                    this.qbGroup.shop = data.shop;
                }
            });
        });
    }

    fileEvent(event: any, item) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let files = event.target.files;
            let file = files[0];
            reader.readAsDataURL(file);
            this.loading = true;
            reader.onload = () => {
                const data = {
                    filename: file.name,
                    filetype: file.type,
                    value: reader.result,
                };
                this.groupService.upFile(data, this.shopDomain).subscribe(res => {
                    this.loading = false;
                    item.recommend.image = res.url;
                }, err => {
                    this.messageService.flashError(err);
                });
            };
        }
    }

    resetRecommendImage(item) {
        item.recommend.image = this.customImageSize(item.image_url);
    }

    onSubmit() {
        if (!this.canSubmit()) {
            return;
        }

        this.loading = true;
        this.btnSave = "Processing...";
        this.messageService.flashInfo("Processing. Please wait...");

        if (this.qbGroup.status == 0) {
            this.qbGroup.status = 1;
        }

        this.qbGroup.name = this.qbGroup.name.trim();

        const obversable = this.groupID
            ? this.groupService.update(this.groupID, this.shopDomain, this.qbGroup)
            : this.groupService.create(this.qbGroup);
        obversable.subscribe(res => {
            this.messageService.flashSuccess('Success.');
            this.router.navigate(['/admin']);
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        if (!this.qbGroup.name) {
            return this.messageService.flashError("Internal Name is required.");
        }
        if (!this.qbGroup.price_type) {
            return this.messageService.flashError("Discount Type is required.");
        }
        if (Object.keys(this.qbGroup.price_levels).length === 0) {
            return this.messageService.flashError("Please add price level.");
        }
        if (Object.keys(this.qbGroup.products).length === 0) {
            return this.messageService.flashError("Please select at least a product.");
        }
        return true;
    }

    addQbLevel() {
        if (!this.addQty || !this.addValue || this.addQty <= 1) {
            return this.messageService.flashWarning("Qty and Percent Off Per Item must both be numbers. Qty must be greater than 1.");
        }
        if (!Number.isInteger(this.addQty)) {
            return this.messageService.flashWarning("Qty must be integer and greater than 1.");
        }
        this.qbGroup.price_levels.push({
            qty: this.addQty,
            value: this.addValue,
        });

        this.quantityModify();
        this.clearAddLevel();
    }

    clearAddLevel() {
        this.addQty = this.addValue = null;
    }

    deleteLevel(index) {
        this.qbGroup.price_levels.splice(index, 1);
        this.quantityModify();
    }

    quantityModify() {
        let data = this.qbGroup.price_levels;
        data = data.sort(function (a, b) { return a.qty - b.qty });
        const dataLength = data.length;
        let prevQty = 1;
        for (let i = 0; i <= dataLength; i++) {
            let qty = data[i] ? data[i].qty : null;
            if (i == 0) {
                this.qtyArr[qty] = qty;
                prevQty = qty;
            } else {
                if (qty && (qty - prevQty) > 1) {
                    this.qtyArr[prevQty] = `${prevQty} &rarr; ${qty - 1}`;
                }
                if (!qty) {
                    this.qtyArr[prevQty] = `${prevQty} &rarr; &prop;`;
                }
                if (qty) this.qtyArr[qty] = qty;
                prevQty = qty;
            }
        }
    }

    selectProducts() {
        const modal = this.modalService.show(AddProductModalComponent);
        (<AddProductModalComponent>modal.content).showModal(this.shopDomain, this.qbGroup.products);
        (<AddProductModalComponent>modal.content).onClose.subscribe(res => {
            (<AddProductModalComponent>modal.content).onComplete.subscribe(results => {
                this.qbGroup.products = _.map(results, p => {
                    p.selected = true;
                    return p;
                });
            });
        });
    }

    saveAllProducts() {
        const mess = `Updating already existing products. Please wait for few minutes.`;
        this.messageService.flashInfo(mess, { timeOut: 5000 });
        this.cookieService.set('__qb_group_message', mess, 1);
        this.groupService.updateAll(this.shopDomain, { price_levels: this.qbGroup.price_levels }).subscribe(res => {
            this.messageService.flashSuccess('Updated already existing products successfully!');
            this.cookieService.delete('__qb_group_message');
        }, err => {
            this.messageService.flashError(err);
        });
    }

    customImageSize(path, size = 'medium') {
        var fileName = path.split('/').pop();
        var f_name = fileName.split('.').slice(0, -1).join('.');
        var f_ext = fileName.split('.').pop();
        return path.replace(f_name, f_name + '_' + size);
    }

    goBack() {
        this._location.back();
    }

    ngOnDestroy() {
        this.subsInit();
    }

    subsInit() {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }

}