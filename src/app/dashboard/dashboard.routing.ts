import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ShopGuardService } from '../shared/services/shop-guard.service';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { DashboardComponent } from './components/dashboard.component';
import { GroupsComponent } from './components/groups.component';
import { CreateGroupComponent } from './components/CURD/create-group.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/dashboard',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
            title: 'Dashboard',
        }
    },
    {
        path: 'admin',
        component: GroupsComponent,
        data: {
            title: 'Quantity Break Groups',
        }
    },
    {
        path: 'create_group',
        component: CreateGroupComponent,
        data: {
            title: 'Create New Group',
        }
    },
    {
        path: 'edit_group/:id',
        component: CreateGroupComponent,
        data: {
            title: 'Edit Group',
        }
    },
]

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ],
})

export class DashboardRoutingModule { }