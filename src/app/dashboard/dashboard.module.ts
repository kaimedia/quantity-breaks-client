import { NgModule } from '@angular/core';
import { FormsModule, FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpModule } from "@angular/http";
import { SharedModule } from '../shared/shared.module';
import { DashboardRoutingModule } from './dashboard.routing';
import { Select2Module } from 'ng2-select2';
import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';
import { DataFilterPipe } from '../shared/components/data-filter-pipe.component';
import { SortPipe } from "../shared/components/sort-pipe.component";
import { CreateGroupComponent } from './components/CURD/create-group.component';
import { ConfirmationModalComponent } from './components/modals/confirm.component';
import { AddProductModalComponent } from './components/modals/add-products.component';
import { DashboardComponent } from './components/dashboard.component';
import { GroupsComponent } from './components/groups.component';

@NgModule({
    declarations: [
        DataFilterPipe,
        SortPipe,
        ConfirmationModalComponent,
        AddProductModalComponent,
        CreateGroupComponent,
        DashboardComponent,
        GroupsComponent,
    ],
    imports: [
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        HttpModule,
        SharedModule,
        DashboardRoutingModule,
        Select2Module,
        BootstrapSwitchModule.forRoot(),
    ],
    exports: [

    ],
    providers: [
        FormBuilder,
    ],
    entryComponents: [
        ConfirmationModalComponent,
        CreateGroupComponent,
        AddProductModalComponent,
    ]
})

export class DashboardModule { }