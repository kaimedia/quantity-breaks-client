import { NgModule } from '@angular/core';
import { SelectivePreloadingStrategy } from './strategies/selective-preload.strategy';

import { StorageService } from './services/storage.service';
import { CookieService } from './services/cookie.service';

@NgModule({
    imports: [

    ],
    exports: [
    ],
    declarations: [
    ],
    providers: [
        SelectivePreloadingStrategy,
        StorageService,
        CookieService,
    ],
})
export class CoreModule { }
