import { Component, OnInit, OnDestroy } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './templates/authentication.component.html',
})

export class AuthenticationComponent implements OnInit, OnDestroy {
    constructor() { }

    ngOnInit() {
        document.querySelector('body').classList.add('login');
    }
    ngOnDestroy(): void {
        document.querySelector('body').classList.remove('login');
    }
}