import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { RoleService } from '../../shared/services/role.service';

@Component({
    selector: 'layout-sidebar',
    templateUrl: './templates/sidebar.component.html'
})

export class SidebarComponent implements OnInit {

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private roleService: RoleService,
    ) { }

    ngOnInit() {
        this.activatedRoute.data.subscribe((data: { user }) => {
            this.roleService.addRole(data.user.role);
        });
    }
}