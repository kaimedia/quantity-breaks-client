import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticatedComponent } from './layout/components/authenticated.component';
import { SelectivePreloadingStrategy } from './core/strategies/selective-preload.strategy';
import { NgxPermissionsGuard } from 'ngx-permissions';
import { ShopGuardService } from './shared/services/shop-guard.service';
import { AuthGuardService } from './shared/services/auth-guard.service';
import { UnAuthGuardService } from './shared/services/un-auth-guard.service';
import { ErrorHandlerComponent } from './shared/components/error-handler.component';
import { SelectShopComponent } from './shared/components/select-shop.component';
import { UserResolver } from './shared/resolves/user.resolver';
import { ShopResolver } from './shared/resolves/shop.resolver';

const routes: Routes = [
    {
        path: '',
        component: AuthenticatedComponent,
        canActivate: [ShopGuardService, AuthGuardService],
        canActivateChild: [NgxPermissionsGuard],
        children: [
            {
                path: '',
                loadChildren: 'app/dashboard/dashboard.module#DashboardModule',
            },
            {
                path: 'user',
                loadChildren: 'app/user/user.module#UserModule',
            },
        ],
        resolve: {
            user: UserResolver,
            shop: ShopResolver,
        },
        data: {
            preload: true
        }
    },
    {
        path: 'auth',
        loadChildren: 'app/authentication/authentication.module#AuthenticationModule',
        canActivate: [UnAuthGuardService]
    },
    {
        path: 'error',
        component: ErrorHandlerComponent,
    },
    {
        path: 'error/:code',
        component: ErrorHandlerComponent,
    },
    {
        path: 'shops',
        component: SelectShopComponent,
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes),
    ],
    exports: [
        RouterModule,
    ],
    providers: [
        SelectivePreloadingStrategy,
    ]
})

export class AppRouting { }