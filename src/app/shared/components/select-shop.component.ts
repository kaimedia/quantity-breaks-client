import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageFlashService } from '../services/message-flash.service';
import { ShopService } from '../services/shop.service';
import { environment } from '../../../environments/environment';

@Component({
    moduleId: module.id,
    selector: 'select-shops',
    templateUrl: './templates/select-shops.component.html',
})

export class SelectShopComponent implements OnInit {
    shops;
    apiUrl;
    constructor(
        private route: Router,
        private activatedRoute: ActivatedRoute,
        private messageService: MessageFlashService,
        private shopService: ShopService,
    ) { }

    ngOnInit() {
        this.apiUrl = environment.API_ROOT;
        this.shopService.list().subscribe(response => {
            this.shops = response;
        }, err => this.messageService.flashError(err));
    }

}
