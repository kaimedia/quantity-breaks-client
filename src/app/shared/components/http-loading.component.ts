import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'http-loading',
    template: `
    <div *ngIf="showLoading" class="request-loading">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
    `,
})

export class HttpLoadingComponent implements OnInit {
    @Input() showLoading: boolean = false;

    constructor() {

    }

    ngOnInit(): void {

    }
}