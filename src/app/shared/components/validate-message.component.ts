import { Component, Input, OnInit } from '@angular/core';
import { RegexHelper } from '../../shared/helpers/regex.helper';
import { ErrorHelper } from '../../shared/helpers/errors.helper';

@Component({
    selector: 'validate-message',
    template: `<ul class="error-text" *ngIf="hasErrors() && (input.dirty || submited)">
    <li *ngIf="input.errors && input.errors.required">{{errorHelper.required(label)}}</li>
    <li *ngIf="input.errors && input.errors.email && input.value">{{errorHelper.invalidEmail(label)}}</li>
    <li *ngIf="input.errors && input.errors.pattern">{{errorHelper.pattern(invalidMessage,label)}}</li>
    <li *ngIf="input.errors && input.errors.minlength">{{errorHelper.minlength(minlength,label)}}</li>
    <li *ngIf="input.errors && input.errors.maxlength">{{errorHelper.maxlength(maxlength,label)}}</li>
    <li *ngIf="equal && input.value !== equal && input.value">{{errorHelper.notequal(label,equalTo,invalidMessage)}}</li>
</ul>`,
})
export class ValidateMessage implements OnInit {
    @Input() input: any;
    @Input() label: string;
    @Input() minlength: number;
    @Input() maxlength: number;
    @Input() equal: string;
    @Input() equalTo: string;
    @Input() invalidMessage: string;
    @Input() submited: boolean = false;
    private regexHelper = RegexHelper;
    private errorHelper = ErrorHelper;

    ngOnInit(): void {
    }

    hasErrors(): boolean {
        if (this.equal && this.input.value !== this.equal) {
            return true;
        }
        return this.input.invalid;
    }
}