import { Injectable } from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { StorageService } from "../../core/services/storage.service";

@Injectable()
export class TokenService {
    token: string = 'token';
    userId: string = 'user_id';
    constructor(
        private storageService: StorageService,
    ) { }
    setToken(token) {
        this.storageService.set(this.token, token);
    }

    getToken() {
        return this.storageService.get(this.token);
    }

    removeToken() {
        this.storageService.remove(this.token);
    }

    isTokenValid(token: string = null) {
        if (!token) {
            token = this.getToken();
        }
        if (token) {
            return tokenNotExpired(null, token);
        }
        return false;
    }

    setUserId(userId) {
        this.storageService.set(this.userId, userId);
    }

    removeUserId() {
        this.storageService.remove(this.userId);
    }

    getUserId() {
        return this.storageService.get(this.userId);
    }

    logOut() {
        this.removeUserId();
        this.removeToken();
    }
}