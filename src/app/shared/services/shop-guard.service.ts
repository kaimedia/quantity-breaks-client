import { Injectable } from '@angular/core';
import { Router, CanActivate, CanDeactivate, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { StorageService } from "../../core/services/storage.service";
import { Location } from '@angular/common';

@Injectable()
export class ShopGuardService implements CanActivate {
    shop;

    constructor(
        private router: Router,
        private activatedRoute: ActivatedRoute,
        private storageService: StorageService,
        private location: Location
    ) {

    }

    get(): Observable<any> {
        return Observable.of(this.getShopDomain());
    }

    canActivate(): Observable<boolean> | boolean {

        this.setShopTokens();

        if (!this.verifyShop()) {
            this.router.navigate(['/shops']);
            return false;
        }

        return true;
    }

    verifyShop() {
        if (!this.getShopDomain()) {
            return false;
        }
        return true;
    }

    setShopTokens() {
        const shop = this.getParameterByName('shop');
        if (shop) {
            this.storageService.set('x-qb-shop', this.getParameterByName('shop'));
        }
    }

    getParameterByName(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regexS = "[\\?&]" + name + "=([^&#]*)";
        var regex = new RegExp(regexS);
        var results = regex.exec(this.location.path());
        return results == null ? null : results[1];
    }

    getShopDomain() {
        const shop = this.storageService.get('x-qb-shop');
        return shop;
    }
}