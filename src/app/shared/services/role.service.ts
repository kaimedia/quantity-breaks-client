import { Injectable } from '@angular/core';
import { NgxRolesService, NgxPermissionsService } from 'ngx-permissions';

@Injectable()
export class RoleService {
    constructor(
        private rolesService: NgxRolesService,
        private permissionsService: NgxPermissionsService,
    ) { }

    addRole(role) {
        this.rolesService.addRole(role, () => {
            return true;
        })
    }

    getRole(role) {
        return this.rolesService.getRole(role);
    }

    getRoles() {
        return this.rolesService.getRoles();
    }

    removeRole(role) {
        this.rolesService.removeRole(role);
    }

    removeAllRoles() {
        this.rolesService.flushRoles();
    }
}