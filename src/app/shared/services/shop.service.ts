import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';
import { Shop } from '../../models/shop';

@Injectable()
export class ShopService {
    constructor(
        private apiConnector: APIConnector,
    ) {

    }

    list(): Observable<Shop> {
        return this.apiConnector.requestAPI('get', '/shop/list');
    }
}