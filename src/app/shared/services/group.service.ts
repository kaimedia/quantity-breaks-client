import { Injectable } from '@angular/core';
import { APIConnector } from '../services/api-connector.service';
import { Observable } from 'rxjs/Rx';
import { Group } from '../../models/group';

@Injectable()
export class GroupService {
    constructor(
        private apiConnector: APIConnector,
    ) {

    }

    list(shop): Observable<any> {
        return this.apiConnector.requestAPI('get', `/batches/${shop}/list/`);
    }

    get(id, shop): Observable<Group> {
        return this.apiConnector.requestAPI('get', `/batches/${shop}/${id}`);
    }

    create(data) {
        return this.apiConnector.requestAPI('post', '/batches', data);
    }

    update(id, shop, data): Observable<any> {
        data.updated_at = new Date();
        return this.apiConnector.requestAPI('patch', `/batches/${shop}/${id}`, data);
    }

    updateAll(shop, data): Observable<any> {
        return this.apiConnector.requestAPI('patch', `/batches/${shop}`, data);
    }

    remove(id, shop) {
        return this.apiConnector.requestAPI('delete', `/batches/${shop}/${id}`);
    }

    upFile(file, shop) {
        return this.apiConnector.requestAPI('post', `/file/${shop}`, file);
    }
}