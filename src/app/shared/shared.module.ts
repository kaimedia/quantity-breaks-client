import { NgModule } from '@angular/core';
import { BrowserXhr } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgxPermissionsModule } from 'ngx-permissions';
import { ModalModule } from 'ngx-bootstrap';
import { LoadingXhrProvider } from './providers/loading-xhr.provider';

import { TokenService } from './services/token.service';
import { RoleService } from './services/role.service';
import { APIConnector } from './services/api-connector.service';
import { AuthGuardService } from './services/auth-guard.service';
import { UnAuthGuardService } from './services/un-auth-guard.service';
import { DateRangeService } from "./services/date-range.service";
import { ShopGuardService } from "./services/shop-guard.service";
import { UserService } from './services/user.service';
import { ProductService } from './services/product.service';
import { ShopService } from './services/shop.service';
import { GroupService } from './services/group.service';
import { PagerService } from './services/pager.service';

import { UserResolver } from './resolves/user.resolver';
import { ShopResolver } from './resolves/shop.resolver';

import { PageLoadingComponent } from './components/page-loading.component';
import { LoadingSpinnerComponent } from './components/loading-spinner.component';
import { HttpLoadingComponent } from './components/http-loading.component';
import { MessageFlashComponent } from './components/message-flash.component';
import { ValidateMessage } from './components/validate-message.component';
import { ErrorHandlerComponent } from './components/error-handler.component';
import { SelectCountryComponent } from './components/select-country.component';
import { SelectCountryCodeComponent } from './components/select-country-code.component';
import { SelectShopComponent } from './components/select-shop.component';

@NgModule({
    declarations: [
        PageLoadingComponent,
        LoadingSpinnerComponent,
        HttpLoadingComponent,
        MessageFlashComponent,
        ValidateMessage,
        ErrorHandlerComponent,
        SelectCountryComponent,
        SelectCountryCodeComponent,
        SelectShopComponent,
    ],
    imports: [
        FormsModule,
        CommonModule,
        NgxPermissionsModule.forRoot(),
        ModalModule.forRoot(),
    ],
    exports: [
        NgxPermissionsModule,
        PageLoadingComponent,
        LoadingSpinnerComponent,
        HttpLoadingComponent,
        MessageFlashComponent,
        ValidateMessage,
        ErrorHandlerComponent,
        SelectCountryComponent,
        SelectCountryCodeComponent,
        SelectShopComponent,
    ],
    providers: [
        {
            provide: BrowserXhr, useClass: LoadingXhrProvider
        },
        RoleService,
        APIConnector,
        DateRangeService,
        TokenService,
        AuthGuardService,
        UnAuthGuardService,
        UserService,
        UserResolver,
        ShopResolver,
        ShopGuardService,
        ShopService,
        ProductService,
        GroupService,
        PagerService,
    ],
})

export class SharedModule { }