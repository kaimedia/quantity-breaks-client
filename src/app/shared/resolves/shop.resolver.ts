import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { ShopGuardService } from '../services/shop-guard.service';

@Injectable()
export class ShopResolver implements Resolve<any> {
    constructor(private shopService: ShopGuardService) {
    }
    resolve(): Promise<any> {
        return this.shopService.get().toPromise();
    }
}