import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { UserRouting } from './user.routing';
import { UserComponent } from './components/user.component';
import { ProfileComponent } from './components/profile.component';
import { ChangePasswordComponent } from './components/change-password.component';
import { ChangeRoleComponent } from './components/change-role.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        UserRouting
    ],
    exports: [
    ],
    declarations: [
        UserComponent,
        ProfileComponent,
        ChangePasswordComponent,
        ChangeRoleComponent,
    ],
    providers: []
})
export class UserModule { }
