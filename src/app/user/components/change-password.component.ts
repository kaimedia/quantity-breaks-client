import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { UserService, User } from '../../shared/services/user.service';
import { MessageFlashService } from '../../shared/services/message-flash.service';
import { ActivatedRoute } from '@angular/router';
import { RegexHelper } from '../../shared/helpers/regex.helper';

@Component({
    moduleId: module.id,
    selector: 'user-change-password',
    templateUrl: './templates/change-password.component.html',
})
export class ChangePasswordComponent implements OnInit {
    @ViewChild('changePassForm') changePassForm;

    user = new User();
    subUser: any;
    regexHelper = RegexHelper;

    constructor(
        private activatedRoute: ActivatedRoute,
        private userService: UserService,
        private messageService: MessageFlashService,
    ) { }

    ngOnInit() {
        this.subUser = this.activatedRoute.parent.parent.parent.data.subscribe((data: { user: User }) => {
            this.user = data.user;
        });
    }

    ngOnDestroy() {
        this.subUser.unsubscribe();
    }

    onSubmit() {
        if (!this.canSubmit()) {
            return false;
        }

        const userData = {
            _id: this.user._id,
            newPassword: this.user.newPassword,
            passwordConfirmation: this.user.passwordConfirmation,
        };

        this.userService.changePassword(userData).subscribe(user => {
            this.messageService.flashSuccess('Changed password successfully');
            this.changePassForm.reset();
        }, err => {
            this.messageService.flashError(err);
        });
    }

    canSubmit() {
        return this.user.newPassword && this.user.passwordConfirmation && this.user.newPassword === this.user.passwordConfirmation
    }

}