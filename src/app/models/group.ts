export class Group {
    _id: string;
    shop: string;
    name: string;
    price_type: string;
    across_variants: string;
    price_levels: object;
    products: object;
    productIds: string;
    status: number;
    created_at: Date;
    updated_at: Date;
}