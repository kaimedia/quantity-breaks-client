const config = require('./config.json');
export const environment = {
  production: true,
  APP_NAME: config.APP_NAME,
  API_ROOT: "https://api-qb.bluesails.net",
  API_PATH: "https://api-qb.bluesails.net/api",
  timeZone: config.timeZone
};
